package main;

public class Active {

    SuffixNode activeNode;
    int activeEdge;
    int activeLength;

    Active(SuffixNode node)
    {
        activeLength = 0;
        activeNode = node;
        activeEdge = -1;
    }

    @Override
    public String toString()
    {
        return "main.Active [activeNode=" +activeNode + ", activeIndex= " + activeEdge + ", activeLength= " + activeLength + "]";
    }

}
