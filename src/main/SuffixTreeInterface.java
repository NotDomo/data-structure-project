package main;

public interface SuffixTreeInterface {

    // Creates SuffixTree
     void build();

     // Traversal of the tree (using DFS)
    void dfsTraversal();


    // Validates input/output
    boolean validate();

}
