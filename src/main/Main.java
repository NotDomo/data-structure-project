package main;

public class Main
{

    public static void main(String[] args) {
        String stringA = "sraigtasparnis";
        String combined = stringA + "$";

        SuffixTree st = new SuffixTree(combined);
        st.build();
        st.dfsTraversal();
        st.validate();
        if (!st.foundPalindrome)
            System.out.println("No palindromes were found in the given string.");

    }
}
